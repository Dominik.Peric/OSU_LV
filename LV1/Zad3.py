#Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
#sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
#potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
#(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku

numbers = []

while(True):
    unos = input("")
    if unos == "Done":
        break
    try:
        num = float(unos)
        numbers.append(num)
    except:
        print("Krivi unos!")
        
print("Korisnik je unio: ",len(numbers),"vrijednosti")
print("Srednja vrijednost: ",sum(numbers)/len(numbers))
print("Minimalna vrijednost: ",min(numbers))
print("Maksimalna vrijednost: ",max(numbers))

numbers.sort()
print("Sortirana lista: ", numbers)
#Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ SMSSpamCollection.txt
#[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozna ˇ cene kao ˇ spam, a neke kao ham.
#Primjer dijela datoteke:
#   ham Yup next stop.
#   ham Ok lar... Joking wif u oni...
#   spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff!
#a) Izracunajte koliki je prosje ˇ can broj rije ˇ ci u SMS porukama koje su tipa ham, a koliko je ˇ
#prosjecan broj rije ˇ ci u porukama koje su tipa spam. ˇ
#b) Koliko SMS poruka koje su tipa spam završava usklicnikom ? 

fhand = open("SMSSpamCollection.txt")

hamCounter = []
spamCounter = []
spamCounterExclamation = 0

for line in fhand:
    line = line.rstrip()
    words = line.split()
    if words[0] == "ham":
        hamCounter.append(len(words)-1)
    if words[0] == "spam":
        if words[-1].endswith("!"):
            spamCounterExclamation = spamCounterExclamation + 1
        spamCounter.append(len(words)-1)

hamWords = sum(hamCounter) / len(hamCounter)
spamWords = sum(spamCounter) / len(spamCounter)

print("Ham, Spam, Spam with exclamation: ",hamWords,spamWords,spamCounterExclamation)



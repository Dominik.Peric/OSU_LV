#Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
#Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci koje se pojavljuju u ˇ
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) pojavljuje u datoteci. ˇ
#Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih

word_count = {}
unique_words = 0

fhand = open("song.txt")
for line in fhand:
    line = line.rstrip()
    words = line.split()
    for word in words:
        word_count[word] = word_count.get(word,0) + 1  #Ako se riječ već nalazi u rječniku, dodaje 1 na njezinu postojeću vrijednost. Ako se ne nalazi, postavlja vrijednost na 1.

for count in word_count.values():
    if count == 1:
        unique_words+=1
print("Broj jedinstvenih rijeci: ", unique_words)

for word, count in word_count.items():
    if count==1:
        print(word)

fhand.close()

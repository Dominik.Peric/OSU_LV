#Napišite programski kod koji ce prikazati sljedece vizualizacije:

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

to_convert = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']
data[to_convert] = data[to_convert].astype('category')

#a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.

plt.hist(data['CO2 Emissions (g/km)'], bins= 20, color='lightblue')
#data['CO2 Emissions (g/km)'].plot(kind='hist', bins = 20)
plt.title('CO2 Emissions (g/km)')
plt.show()

#b) Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije ¯
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu¯
#velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva.

data.plot.scatter(x='Fuel Consumption City (L/100km)',
                  y='CO2 Emissions (g/km)',
                  c='Fuel Type',
                  cmap ="Reds")

plt.show()

#c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip ´
#goriva. Primjecujete li grubu mjernu pogrešku u podacima?

data.plot.box(column='Fuel Consumption Hwy (L/100km)', by="Fuel Type")
plt.show()

#d) Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu ˇ
#groupby

grouped_data = data.groupby("Fuel Type")['Fuel Type'].count()
grouped_data.plot.bar()
plt.title("Broj vozila po tipu goriva")
plt.show()

#e) Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na ˇ
#broj cilindara.

grouped_data = data.groupby("Cylinders")['CO2 Emissions (g/km)'].mean()
grouped_data.plot.bar()
plt.title("CO2 prema broju cilindara")
plt.show()

#Skripta zadatak_1.py ucitava podatkovni skup iz ˇ data_C02_emission.csv.
#Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljede ´ ca pitanja:

import pandas as pd
import numpy as np

data = pd.read_csv('data_C02_emission.csv')

#a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili ˇ
#duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke veli ˇ cine konvertirajte u tip ˇ
#category.

print(f"DataFrame sadrzi: {len(data)} mjerenja!")
print(f"Tipovi velicina: \n{data.dtypes}")
print("Null: " + str(data.isnull().sum()))
print("Duplicirane: " + str(data.duplicated().sum()))

#Kategoricke veli ˇ cine konvertirajte u tip category.
to_convert = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']
data[to_convert] = data[to_convert].astype('category')
print(data.dtypes)

#b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal: ´
#ime proizvoda¯ ca, model vozila i kolika je gradska potrošnja

sorted_data = data.sort_values(by=['Fuel Consumption City (L/100km)'])
print(f"Automobili s najmanjom gradskom potrosnjom: \n {sorted_data[['Make','Model','Fuel Consumption City (L/100km)']].head(3)}")
print(f"Automobili s najvecom gradskom potrosnjom: \n {sorted_data[['Make','Model','Fuel Consumption City (L/100km)']].tail(3)}")

#c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosje ¯ cna C02 emisija ˇ
#plinova za ova vozila?

filtered_data = data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] <3.5)]
print(f"Vozila s motorima [2.5, 3.5]: " + str(len(filtered_data)))
print(f"Prosjecna emisija CO2 plinova za vozila s motorima [2.5, 3.5]: {filtered_data['CO2 Emissions (g/km)'].mean()}")

#d) Koliko mjerenja se odnosi na vozila proizvoda¯ ca Audi? Kolika je prosjecna emisija C02 ˇ
#plinova automobila proizvoda¯ ca Audi koji imaju 4 cilindara?

audi = data[data['Make'] == "Audi"]
print(f"Audi: " + str(len(audi)))
audi_4_cilindra = audi[audi['Cylinders'] == 4]
print(f"Prosjecna CO2 emisija plinova za Audi s 4 cilindra: {audi_4_cilindra['CO2 Emissions (g/km)'].mean()}")

#e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na ˇ
#broj cilindara?

data_groupedby_cylinders = data.groupby('Cylinders')
print(f"Broj vozila prema broju cilindara: {data_groupedby_cylinders.size()}") #cilindri 3,4,5,6,8,10,12,16
data_groupedby_cylinders_even = data[data['Cylinders'] % 2 == 0].groupby('Cylinders')
print(f"Broj vozila s parnim brojem cilindara: {data_groupedby_cylinders_even.size()}")

#print("4 cilindra")
#data_groupedby_cylinders = data[data['Cylinders'] == 4]
#print(len(data_groupedby_cylinders))
#print(data_groupedby_cylinders['CO2 Emissions (g/km)'].mean())

print(f"Prosjecna emisija CO2 Plinova prema broju cilindara: \n{data_groupedby_cylinders['CO2 Emissions (g/km)'].mean()}")

#f) Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila ˇ
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

dizel = data[data['Fuel Type'] == 'D']
print(f"Prosjecna gradska potrosnja za dizel: {dizel['Fuel Consumption City (L/100km)'].mean()}")
print(f"Medijalna vrijednost: {dizel['Fuel Consumption City (L/100km)'].median()}")

regularni_benzin = data[data['Fuel Type'] == 'X']
print(f"Prosjecna gradska potrosnja za regularni benzin: {regularni_benzin['Fuel Consumption City (L/100km)'].mean()}")
print(f"Medijalna vrijednost: {regularni_benzin['Fuel Consumption City (L/100km)'].median()}")

#g)Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva? 

dizel_najvecaPotrosnja = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]
dizel_najvecaPotrosnja = dizel_najvecaPotrosnja.sort_values(by="Fuel Consumption City (L/100km)")
print(f"Dizelsko vozilo s 4 cilindra s najvecom gradskom potrosnjom: {dizel_najvecaPotrosnja.tail(1)}")

#h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?

manual_transmission = data[data['Transmission'].str.startswith('M')]
print(f"Rucni mijenjac imaju: {len(manual_transmission)} vozila.")

#i) Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.

print("Korelacija:")
print(data.corr(numeric_only=True))

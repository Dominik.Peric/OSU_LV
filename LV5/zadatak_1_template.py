#Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem s dvije
#ulazne velicine. Podaci su podijeljeni na skup za ucenje i skup za testiranje modela. 

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn . metrics import confusion_matrix , classification_report, ConfusionMatrixDisplay, accuracy_score, precision_score, recall_score


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


#a) Prikažite podatke za ucenje u ˇ x1 −x2 ravnini matplotlib biblioteke pri cemu podatke obojite ˇ
#s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
#marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
#cmap kojima je moguce de ´ finirati boju svake klase

plt.figure()
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap='plasma')
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker='x', cmap='plasma')
plt.show()

#b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa poda- ´
#taka za ucenje.

LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

#c) Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke ¯
#naucenog modela u ravnini ˇ x1 − x2 zajedno s podacima za ucenje. Napomena: granica ˇ
#odluke u ravnini x1 −x2 definirana je kao krivulja: θ0 +θ1x1 +θ2x2 = 0.

plt.plot(X_train[:,1], -LogRegression_model.coef_[0, 1]/LogRegression_model.coef_[0, 0]*X_train[:,1] - LogRegression_model.intercept_[0]*LogRegression_model.coef_[0, 0])
plt.show()


#d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke ˇ
#regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunajte tocnost, ˇ
#preciznost i odziv na skupu podataka za testiranje.

y_test_p = LogRegression_model.predict(X_test)

cm = confusion_matrix(y_test, y_test_p)
print("Matrica zabune:")
print(cm)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test ,y_test_p))
disp.plot()
plt.show()

print("Tocnost:", accuracy_score(y_test ,y_test_p))
print (classification_report(y_test ,y_test_p))
#print("Preciznost:", precision_score(y_test, y_test_p))
#print("Odziv:", recall_score(y_test, y_pred))

#e) Prikažite skup za testiranje u ravnini x1 −x2. Zelenom bojom oznacite dobro klasificirane
#primjere dok pogrešno klasificirane primjere oznacite crnom bojom

y_test_p = LogRegression_model.predict(X_test)

correct_indices = np.where(y_test_p == y_test)[0]
incorrect_indices = np.where(y_test_p != y_test)[0]

plt.figure()
plt.scatter(X_test[correct_indices, 0], X_test[correct_indices, 1], c='green', label='Dobro klasificirani')
plt.scatter(X_test[incorrect_indices, 0], X_test[incorrect_indices, 1], c='black', label='Pogresno klasificirani')
plt.xlabel('X1')
plt.ylabel('X2')
plt.show()

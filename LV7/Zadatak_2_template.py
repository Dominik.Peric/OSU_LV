import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs_\imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()


#Kvantizacija boje je proces smanjivanja broja razlicitih boja u digitalnoj slici, ali ˇ
#uzimajuci u obzir da rezultantna slika vizualno bude što slicnija originalnoj slici. Jednostavan ˇ
#nacin kvantizacije boje može se postici primjenom algoritma ´ K srednjih vrijednosti na RGB
#vrijednosti elemenata originalne slike. Kvantizacija se tada postiže zamjenom vrijednosti svakog
#elementa originalne slike s njemu najbližim centrom. Na slici 7.3a dan je primjer originalne
#slike koja sadrži ukupno 106,276 boja, dok je na slici 7.3b prikazana rezultantna slika nakon
#kvantizacije i koja sadrži samo 5 boja koje su odredene algoritmom ¯ K srednjih vrijednosti.

#1. Otvorite skriptu zadatak_2.py. Ova skripta ucitava originalnu RGB sliku ˇ test_1.jpg
#te ju transformira u podatkovni skup koji dimenzijama odgovara izrazu (7.2) pri cemu je ˇ n
#broj elemenata slike, a m je jednak 3. Koliko je razlicitih boja prisutno u ovoj slici? ˇ
#2. Primijenite algoritam K srednjih vrijednosti koji ce pronaci grupe u RGB vrijednostima ´
#elemenata originalne slike.
#3. Vrijednost svakog elementa slike originalne slike zamijeni s njemu pripadajucim centrom. ´
#4. Usporedite dobivenu sliku s originalnom. Mijenjate broj grupa K. Komentirajte dobivene
#rezultate.
#5. Primijenite postupak i na ostale dostupne slike.
#6. Graficki prikažite ovisnost ˇ J o broju grupa K. Koristite atribut inertia objekta klase
#KMeans. Možete li uociti lakat koji upu ˇ cuje na optimalni broj grupa? ´
#7. Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. Što
#primjecujete?

km = KMeans(n_clusters=5, init='random', n_init=5, random_state=42)  
km.fit(img_array_aprox)

cluster_centers = km.cluster_centers_   #dobivanje centara boja
labels = km.labels_                     #indeksi grupe 

for i in range(len(img_array)):
    img_array_aprox[i] = cluster_centers[labels[i]]

img_new = np.reshape(img_array_aprox, (w,h,d))    #Oblikovanje slike natrag u originalne dimenzije
img_new = (img_new*255).astype(np.uint8)

plt.figure()
plt.title("Kvantizirana slika")
plt.imshow(img_new)
plt.tight_layout()
plt.show()

J_values = []
for i in range(1,15):
    km = KMeans(n_clusters=i, random_state=42)
    km.fit(img_array)
    J_values.append(km.inertia_)

plt.figure()
plt.plot(range(1,15), J_values, marker=".")
plt.xlabel("K")
plt.ylabel("J")
plt.tight_layout()
plt.show()

unique_labels = np.unique(labels)
for i in range (len(unique_labels)):
    binary_image = labels == unique_labels[i]
    binary_image = np.reshape(binary_image, (w,h))
    plt.figure()
    plt.title(f"Grupa {i+1}")
    plt.imshow(binary_image)
    plt.tight_layout()
    plt.show()

import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering


def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
X = generate_data(500, 1)

# prikazi primjere u obliku dijagrama rasprsenja
plt.figure()
plt.scatter(X[:,0],X[:,1])
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

#1 Skripta zadatak_1.py sadrži funkciju generate_data koja služi za generiranje
#umjetnih podatkovnih primjera kako bi se demonstriralo grupiranje. Funkcija prima cijeli broj
#koji definira željeni broju uzoraka u skupu i cijeli broj od 1 do 5 koji definira na koji nacince´
#se generirati podaci, a vraca generirani skup podataka u obliku numpy polja pricemu su prvi i ˇ
#drugi stupac vrijednosti prve odnosno druge ulazne velicine za svaki podatak. Skripta generira ˇ
#500 podatkovnih primjera i prikazuje ih u obliku dijagrama raspršenja.

#1. Pokrenite skriptu. Prepoznajete li koliko ima grupa u generiranim podacima? Mijenjajte
#nacin generiranja podataka.

#2. Primijenite metodu K srednjih vrijednosti te ponovo prikažite primjere, ali svaki primjer
#obojite ovisno o njegovoj pripadnosti pojedinoj grupi. Nekoliko puta pokrenite programski
#kod. Mijenjate broj K. Što primjecujete?

km = KMeans(n_clusters=5, init='random', n_init=5, random_state=0)
km.fit(X)
labels = km.predict(X)

plt.figure()
plt.scatter(X[:,0],X[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('K=5')
plt.show()

#najbolji k=3, ako je manji od 3 spaja vise grupa u jednu, a ako je veci onda dijeli jednu grupu na vise podgrupa

#3. Mijenjajte nacin definiranja umjetnih primjera te promatrajte rezultate grupiranja podataka
#(koristite optimalni broj grupa). Kako komentirate dobivene rezultate?

km = KMeans(n_clusters=3, init='random', n_init=5, random_state=0)
km.fit(X)
labels = km.predict(X)

plt.figure()
plt.scatter(X[:,0],X[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('K=3')
plt.show()

#generiranje podatkovnih primjera (način 2)
X2 = generate_data(500, 2)
km = KMeans(n_clusters=3, init='random', n_init=5, random_state=0)
km.fit(X2)
labels = km.predict(X2)

plt.figure()
plt.scatter(X2[:,0],X2[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.show()

#generiranje podatkovnih primjera (način 3)
X3 = generate_data(500, 3)
km = KMeans(n_clusters=4, init='random', n_init=5, random_state=0)
km.fit(X3)
labels = km.predict(X3)

plt.figure()
plt.scatter(X3[:,0],X3[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.show()

#generiranje podatkovnih primjera (način 4)
X4 = generate_data(500, 4)
km = KMeans(n_clusters=2, init='random', n_init=5, random_state=0)
km.fit(X4)
labels = km.predict(X4)

plt.figure()
plt.scatter(X4[:,0],X4[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.show()

# generiranje podatkovnih primjera (način 5)
X5 = generate_data(500, 5)
km = KMeans(n_clusters=2, init='random', n_init=5, random_state=0)
km.fit(X5)
labels = km.predict(X5)

plt.figure()
plt.scatter(X5[:,0],X5[:,1], c=labels, cmap='viridis')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.show()

#
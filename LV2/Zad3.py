#Skripta zadatak_3.py ucitava sliku ’ ˇ road.jpg’. Manipulacijom odgovarajuce´
#numpy matrice pokušajte:

import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")

#a) posvijetliti sliku,

imgBrightness = img[:,:,0].copy()  #samo se uzima prvi kanal slike (crveni kanal) jer će promjena svjetline biti primijenjena samo na njega
imgBrightness = np.clip(imgBrightness, 0, 180) #Funkcija np.clip() koristi se kako bi se osiguralo da vrijednosti piksela ostanu u opsegu od 0 do 255, što je raspon vrijednosti za RGB slike
imgBrightness = imgBrightness + 30
plt.imshow(imgBrightness ,cmap ="gray")
plt.title("Povecana svjetlina")
plt.show()

#b) prikazati samo drugu cetvrtinu slike po širini,
secondQuarter = img[:,:,0].copy()
secondQuarter = secondQuarter[::,int(secondQuarter.shape[1]/4):int(secondQuarter.shape[1]/2):]

plt.imshow(secondQuarter ,cmap ="gray")
plt.title("Druga cetvrtina")
plt.show()

#c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu
rotated = img[:,:,0].copy()
rotated = np.rot90(rotated, k=3)

plt.imshow(rotated ,cmap ="gray")
plt.title("Rotirana")
plt.show()

#d) zrcaliti sliku.

mirror = img[:,:,0].copy()
mirror = np.flip(mirror, axis=1)

plt.imshow(mirror ,cmap ="gray")
plt.title("Zrcaljena slika")
plt.show()

#Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
#ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja ˇ data pri cemu je u ˇ
#prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci´
#stupac polja je masa u kg.

import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('data.csv', delimiter=',', skip_header=True) #podaci iz data.csv ucitani u varijablu data

#a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
print("Mjerenja su izvrsena na: ", len(data), "osoba")

#b) Prikažite odnos visine i mase osobe pomocu naredbe ´ matplotlib.pyplot.scatter.
#plt.scatter(x, y)
#plt.show()

height = data[0:, 1]
weight = data[0:, 2]

plt.scatter(height, weight)
plt.xlabel("Height")
plt.ylabel("Weight")
plt.show()

#c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.

height1 = data[::50,1]
weight1 = data[::50,2]

plt.scatter(height1, weight1)
plt.xlabel("Height")
plt.ylabel("Weight")
plt.show()

#d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom ˇ
#podatkovnom skupu

#print("Minimalna visina: ",data[:,1].mean())
print("Srednja visina: ",height.mean())
#print("Minimalna visina: ",data[:,1].min())
print("Minimalna visina: ",min(height))
print("Maksimalna visina: ",max(height))

#e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
#muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
#ind = (data[:,0] == 1)

indm = (data[:,0] == 1)

print("Muškarci:")
print("Srednja visina: " + str(data[indm, 1].mean()))
print("Minimalna visina: " + str(data[indm, 1].min()))
print("Maksimalna visina: " + str(data[indm, 1].max()))


indz = (data[:,0] == 0)

print("Žene:")
print("Srednja visina: " + str(data[indz, 1].mean()))
print("Minimalna visina: " + str(data[indz, 1].min()))
print("Maksimalna visina: " + str(data[indz, 1].max()))



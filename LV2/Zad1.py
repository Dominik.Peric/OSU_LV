#Pomocu funkcija ´ numpy.array i matplotlib.pyplot pokušajte nacrtati sliku
#2.3 u okviru skripte zadatak_1.py. Igrajte se sa slikom, promijenite boju linija, debljinu linije i
#sl.

import numpy as np
import matplotlib.pyplot as plt

plt.plot([1,3], [1,1], 'b', linewidth=1, marker='x', markersize=5)
plt.plot([3,3], [1,2], 'b', linewidth=1, marker='x', markersize=5)
plt.plot([3,2], [2,2], 'b', linewidth=1, marker='x', markersize=5)
plt.plot([2,1], [2,1], 'b', linewidth=1, marker='x', markersize=5)

plt.axis([0, 4, 0, 4])
plt.xlabel('x os')
plt.ylabel('y os')
plt.title("Zad 1")
plt.show()

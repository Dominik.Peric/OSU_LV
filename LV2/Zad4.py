#Napišite program koji ce kreirati sliku koja sadrži ´ cetiri kvadrata crne odnosno ˇ
#bijele boje (vidi primjer slike 2.4 ispod). Za kreiranje ove funkcije koristite numpy funkcije
#zeros i ones kako biste kreirali crna i bijela polja dimenzija 50x50 piksela. Kako biste ih složili
#u odgovarajuci oblik koristite numpy funkcije ´ hstack i vstack.

import numpy as np
import matplotlib.pyplot as plt

white = np.zeros((50, 50))
black = np.ones((50, 50))

black_white = np.hstack((black,white))
white_black = np.hstack((white,black))

img = np.vstack((white_black,black_white))

plt.figure()
plt.imshow(img, cmap="gray")
plt.show()

#Na temelju rješenja prethodnog zadatka izradite model koji koristi i kategoricku ˇ
#varijable „Fuel Type“ kao ulaznu velicinu. Pri tome koristite 1-od-K kodiranje kategorickih ˇ
#velicina. Radi jednostavnosti nemojte skalirati ulazne velicine. Komentirajte dobivene rezultate. ˇ
#Kolika je maksimalna pogreška u procjeni emisije C02 plinova u g/km? O kojem se modelu
#vozila radi?

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
import matplotlib.pyplot as plt
from sklearn.preprocessing import OneHotEncoder
import math

data = pd.read_csv('data_C02_emission.csv')
print(data.dtypes)

#U scikit-learn dostupna je klasa OneHotEncoder s kojom je moguce izvršiti kodiranje ´
#nominalnih kategorickih velicina kao što je prikazanu u isjecku koda u primjeru

ohe = OneHotEncoder()
data_encoded = data[['Make', 'Model', 'Vehicle Class', 'Engine Size (L)', 'Cylinders', 'Transmission', 'Fuel Type', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]

categorical_columns = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']
data_encoded = pd.get_dummies(data, columns=categorical_columns)
y = data[['CO2 Emissions (g/km)']]
X_train, X_test, y_train, y_test = train_test_split(data_encoded, y, test_size=0.2, random_state=1)

linearModel = lm.LinearRegression()
linearModel.fit(X_train ,y_train)
print(f"Koeficijenti linearnog modela: {linearModel.coef_}")
y_test_prediction = linearModel.predict(X_test)


plt.figure()
plt.scatter(X_test['Engine Size (L)'], y_test, s=10, color='blue', label='real data')
plt.scatter(X_test['Engine Size (L)'], y_test_prediction, s=10, color='red', label='predicted data')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.title('Real vs Predicted Data')


MSE = mean_squared_error(y_test, y_test_prediction)
print(f"Mean squared error(MSE): {MSE}")
MAE = mean_absolute_error(y_test, y_test_prediction)
print(f"Mean absolute error(MAE): {MAE}")
MAPE = mean_absolute_percentage_error(y_test, y_test_prediction)
print(f"Mean absolute percentage error(MAPE): {MAPE}")
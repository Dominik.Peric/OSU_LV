#Skripta zadatak_1.py ucitava podatkovni skup iz ˇ data_C02_emission.csv.
#Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju ostalih numerickih ulaznih veli ˇ cina. Detalje oko ovog podatkovnog skupa mogu se prona ˇ ci u 3. ´
#laboratorijskoj vježbi.

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
import matplotlib.pyplot as plt
import math

data = pd.read_csv('data_C02_emission.csv')
print(data.dtypes)

#a) Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite
#podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.

X = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
y = data[['CO2 Emissions (g/km)']]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=1)

#b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova ´
#o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite ˇ
#plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom. 

plt.scatter(x=X_train['Fuel Consumption City (L/100km)'],y=y_train, c='Blue')
plt.scatter(x=X_test['Fuel Consumption City (L/100km)'],y=y_test, c='Red')
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("C02 Emissions(g/km)")
plt.show()

#c) Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti ˇ
#jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja ˇ
#transformirajte ulazne velicine skupa podataka za testiranje.

#sc = MinMaxScaler()
#X_train_scaled = sc.fit_transform(X_train)
#X_train_scaled = pd.DataFrame(X_train_scaled, columns=X_train.columns)
#X_test_scaled = sc.transform(X_test)
#X_test_scaled = pd.DataFrame(X_test_scaled, columns=X_test.columns)
#Linije koda koje stvaraju DataFrameove nakon skaliranja ne moraju biti obavezne ako ne planiramo koristiti DataFrameove za daljnju obradu 

sc = MinMaxScaler()  #Nakon što se primijeni MinMaxScaler, vrijednosti atributa skaliraju se na opseg [0, 1].
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

plt.hist(x=X_train['Fuel Consumption City (L/100km)'], color='blue', bins=5)
plt.xlabel("Fuel Consumption City (L/100km)")
plt.title("Prije skaliranja")
plt.show()

plt.hist(x=X_train_n[:, 1], color='red',bins=5)
plt.xlabel("Fuel Consumption City (L/100km)")
plt.title("Poslije skaliranja")
plt.show()

#d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
#povežite ih s izrazom 4.6.

linearModel = lm.LinearRegression()
linearModel.fit(X_train_n ,y_train)
print(f"Koeficijenti linearnog modela: {linearModel.coef_}")

#e) Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite ˇ
#pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene ˇ
#dobivene modelom

y_test_prediction = linearModel.predict(X_test_n)
plt.scatter(x=y_test, y=y_test_prediction)
plt.title('Stvarne vrijednosti - Predviđene vrijednosti')
plt.xlabel("Stvarne vrijednosti")
plt.ylabel("Predviđene vrijednosti")
plt.show()

#f) Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na ˇ
#skupu podataka za testiranje

MAE = mean_absolute_error( y_test , y_test_prediction)
MSE = mean_squared_error(y_test , y_test_prediction)
MAPE = mean_absolute_percentage_error(y_test , y_test_prediction)
RMSE = math.sqrt(MSE)
R2 = r2_score(y_test , y_test_prediction)
print(f"MSE: {MSE}, MAE: {MAE}, RMSA: {RMSE}, MAPE: {MAPE}, R2: {R2}")
plt.show()

#g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj ¯
#ulaznih velicina?

#S dvije i vise ulaznih velicina model dobro opisuje podatke


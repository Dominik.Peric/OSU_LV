import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model


#Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1 i MNIST skup ¯
#podataka. Pomocu matplotlib biblioteke potrebno je prikazati nekoliko loše klasi ´ ficiranih slika iz
#skupa podataka za testiranje. Pri tome u naslov slike napišite stvarnu oznaku i oznaku predvidenu ¯
#mrežom

model = load_model( 'model/model.keras')
model.summary()

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, 10)
y_test_s = keras.utils.to_categorical(y_test, 10)

x_train_s = np.reshape(x_train_s, (60000, 28*28))
x_test_s = np.reshape(x_test_s, (10000, 28*28))

y_pred = model.predict(x_test_s)
y_pred = np.argmax(y_pred, axis=1)
score = model.evaluate(x_test_s, y_test_s, verbose=0)

for i in range(100):
    if y_test[i] != y_pred[i]:
        plt.figure()
        plt.imshow(x_test[i], cmap='gray')
        plt.title(f"True: {y_test[i]}, Predicted: {y_pred[i]}") 
plt.show()


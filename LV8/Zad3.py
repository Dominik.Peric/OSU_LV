#Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1. Nadalje, skripta ¯
#treba ucitati sliku ˇ test.png sa diska. Dodajte u skriptu kod koji ce prilagoditi sliku za mrežu, ´
#klasificirati sliku pomocu izgradene mreže te ispisati rezultat u terminal. Promijenite sliku ¯
#pomocu nekog grafickog alata (npr. pomo ˇ cu Windows Paint-a nacrtajte broj 2) i ponovo pokrenite ´
#skriptu. Komentirajte dobivene rezultate za razlicite napisane znamenke.

import numpy as np
from tensorflow import keras
from keras import layers, utils
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model
import imageio as iio

num_classes = 10

model = load_model( 'FCN/')
model.summary()

x_test = utils.load_img('test.png', target_size = (28, 28), color_mode = "grayscale")
x_test = utils.img_to_array(x_test)

y_test = 5

# skaliranje slike na raspon [0,1]
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_test_s = np.expand_dims(x_test_s, -1)

# pretvori labele
y_test_s = keras.utils.to_categorical(y_test, num_classes)

x_test_s = np.reshape(x_test_s, (1, 28*28))

y_pred = model.predict(x_test_s)
y_pred = np.argmax(y_pred, axis=1)

plt.figure()
plt.imshow(x_test, cmap='gray')
plt.title(f"True: {y_test}, Predicted: {y_pred}") 
plt.show()

